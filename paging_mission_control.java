import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class paging_mission_control {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		
		
		File file = new File("input.txt");
		
		ArrayList<String> fileinput = new ArrayList<>();
		
		ArrayList<String> fileinputs = new ArrayList<>();
		
		Scanner fileScan = new Scanner(file);
		while(fileScan.hasNextLine())
		{
			StringTokenizer str = new StringTokenizer(fileScan.nextLine(),"|");
			while(str.hasMoreTokens())
			{
				fileinput.add(str.nextToken());
				
			
			}
			
			
			
		}
		
		
		
		
		int satID= 0;
		double redHL = 0;
		double yelHL = 0;
		double yelLL = 0;
		double redLL =0;
		double raw_val =0;
		String comp =null;
		String time =null;
		
		
		for(int x  = 0; x< fileinput.size();x=x+8)
		{
			time  = fileinput.get(x);
			satID = Integer.parseInt(fileinput.get(x+1));
			redHL = Double.parseDouble(fileinput.get(x + 2));
			yelHL = Double.parseDouble(fileinput.get(x + 3));
			yelLL = Double.parseDouble(fileinput.get(x + 4));
			redLL = Double.parseDouble(fileinput.get(x + 5));
			raw_val = Double.parseDouble(fileinput.get(x + 6));
			comp = fileinput.get(x + 7);
			
		
			
				time = time.substring(0,4)+"-"+time.substring(4);
				time = time.substring(0,7)+"-"+time.substring(7);
				time = time.replace(' ', 'T');
				time = time.substring(0,time.length())+"Z"+time.substring(time.length());
				
				
		
			
			if(raw_val < redLL || (raw_val>redLL &&raw_val<yelLL))
			{
				String output =(
					
					"\n"+"{\n"+
					  "\"satelliteId\": "+satID+",\n"+"\"severity\": "+"\""+"RED LOW"+"\""+",\n"+"\"component\": "+"\""+comp+"\""+",\n"+"\"timestamp\":"+"\""+time+"\""+
					
							
							
							
							"\n }"	
					);
				fileinputs.add(output);
			
			}
			else if(raw_val> yelLL && raw_val < yelHL)
			{
				String output =(
						
						"\n"+"{\n"+
						  "\"satelliteId\": "+satID+",\n"+"\"severity\": "+"\""+"YELLOW LOW"+"\""+",\n"+"\"component\": "+"\""+comp+"\""+",\n"+"\"timestamp\":"+"\""+time+"\""+
						
								
								
								
								"\n }"	
						);
				fileinputs.add(output);
				
			}
			else if( raw_val > yelHL && raw_val <redHL)
			{
				String output =(
						
						"\n"+"{\n"+
						  "\"satelliteId\": "+satID+",\n"+"\"severity\": "+"\""+"YELLOW HIGH"+"\""+",\n"+"\"component\": "+"\""+comp+"\""+",\n"+"\"timestamp\":"+"\""+time+"\""+
						
								
								
								
								"\n }"	
						);
				fileinputs.add(output);
			}
			else if(raw_val > redHL)
			{
				String output =(
						
						"\n"+"{\n"+
						  "\"satelliteId\": "+satID+",\n"+"\"severity\": "+"\""+"RED HIGH"+"\""+",\n"+"\"component\": "+"\""+comp+"\""+",\n"+"\"timestamp\":"+"\""+time+"\""+
						
								
								
								
								"\n }"	
						);
				
				
				
				fileinputs.add(output);
				
			}
			
			 satID= 0;
			 redHL = 0;
			 yelHL = 0;
			 yelLL = 0;
			 redLL =0;
			 raw_val =0;
			comp =null;
			time = null;
			
			
		}
		
System.out.println(fileinputs);

	}

}
